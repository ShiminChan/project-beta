import React from "react";
import { useState } from "react"


function TechForm() {
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [employee_id, setEmployeeID ] = useState('')

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const body = {
            first_name: first_name,
            last_name: last_name,
            employee_id: employee_id,
        }

        const postUrl ="http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response = await fetch(postUrl, fetchConfig);
        if (response.ok) {
            setFirstName('')
            setLastName('')
            setEmployeeID('')
        }
    }


    return(
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1 className="text-center">Add a Technician</h1>
                <form id="create-vehicle-form" onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} value={first_name} placeholder="first_name" required type="first_name" name ="first_name" id="first_name" className="form-control"/>
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} value={last_name} placeholder="last_name" required type="last_name" name ="last_name" id="last_name" className="form-control"/>
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIDChange} value={employee_id} placeholder="employee_id" required type="employee_id" name ="employee_id" id="employee_id" className="form-control"/>
                        <label htmlFor="employee_id">Employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create Technician</button>
                </form>
            </div>
        </div>
    </div>
    )
}
export default TechForm

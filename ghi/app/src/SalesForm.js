import React, { useEffect, useState } from 'react';


function SalesForm() {

    const [salespersons, setSalespersons] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [customers, setCustomers] = useState([]);
    const [customer, setCustomer] = useState('');
    const [automobiles, setAutomobiles] = useState([]);
    const [automobile, setAutomobile] = useState('');
    const [price, setPrice] = useState('');


    const fetchSalespersonData = async () => {
        const SalespersonUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(SalespersonUrl);
        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespeople)
        }
    }

    const fetchCustomerData = async () => {
        const CustomerUrl = "http://localhost:8090/api/customers/";
        const response = await fetch(CustomerUrl);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    const fetchAutomobileData = async () => {
        const AutomobileUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(AutomobileUrl);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }

    useEffect ( () => {
        fetchCustomerData();
        fetchAutomobileData();
        fetchSalespersonData();
    }, [])


    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
}


    const handleSubmit = async (event) => {
    event.preventDefault();

    const data ={
        automobile: automobile,
        salesperson: salesperson,
        customer: customer,
        price: price
    }
    const SalesUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(SalesUrl, fetchConfig);
    if (response.ok) {
        setAutomobile('');
        setSalesperson('');
        setCustomer('');
        setPrice('');
    }
}

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sales-form">
                <div className="form-floating mb-3">
                <select onChange={handleAutomobileChange} value={automobile} placeholder="Automobile VIN" required name="automobile" id="automobile" className="form-control">
                <option value="">Choose an automobile VIN...</option>
                {automobiles.map(automobile => {
                    return (
                        <option key={automobile.id} value={automobile.vin}>
                            {automobile.vin}
                        </option>
                    );
                    })}
                </select>
                </div>
                <div className="form-floating mb-3">
                <select onChange={handleSalespersonChange} value={salesperson} placeholder="Salesperson" required name="salesperson" id="salesperson" className="form-control">
                <option value="">Choose a salesperson</option>
                {salespersons.map(saleppl => {
                    return (
                        <option key={saleppl.id} value={saleppl.id}>
                            {saleppl.first_name} {saleppl.last_name}
                        </option>
                    );
                    })}
                </select>
                </div>
                <div className="form-floating mb-3">
                <select onChange={handleCustomerChange} value={customer} placeholder="Customer" required name="customer" id="customer" className="form-control">
                <option value="">Choose a customer</option>
                {customers.map(c => {
                    return (
                        <option key={c.id} value={c.phone_number}>
                            {c.first_name} {c.last_name}
                        </option>
                    );
                    })}
                </select>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handlePriceChange} value={price} placeholder="price" required type="text" name="price" id="price" className="form-control" />
                <label htmlFor="sale">Price</label>
                </div>

                <button className="btn btn-primary">Create</button>
                </form>
        </div>
        </div>
        </div>
    )
}

export default SalesForm;

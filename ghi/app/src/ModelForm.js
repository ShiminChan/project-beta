import React, { useEffect, useState } from 'react';


function ModelForm() {

    const [name, setName] = useState('');
    const [picture, setPicture] = useState('');
    const [manufacturer, setManufacturer] = useState('');

    const handlenameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
    
    const data ={
        name: name,
        picture_url: picture,
        manufacturer_id: manufacturer
    }

    const ModelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(ModelUrl, fetchConfig);
    if (response.ok) {
        const newModel = await response.json();
        console.log(newModel);

        setName('');
        setPicture('');
        setManufacturer('');
    }
}

    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
    const Manu_url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(Manu_url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
  }
}  
  
useEffect( () => {
    fetchData()
}, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Add a Model</h1>
            <form onSubmit={handleSubmit} id="create-model-form">
                <div className="form-floating mb-3">
                <input onChange={handlenameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Model name...</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={picture} alt="" placeholder="picture" required type="text" name="picture" id="picture" className="form-control" />
                <label htmlFor="picture">Picture URL...</label>
                </div>
                <div className="form-floating mb-3">
                <select onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required name="manufacturer" id="manufacturer" className="form-control">
                <option value="">Choose a manufacturer...</option>
                {manufacturers.map(manufacturer => {
                    return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                        </option>
                    );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
        </div>
        </div>
        </div>       
    )
}
export default ModelForm;
import React, { useEffect, useState } from "react";


function ServiceHistory (){
   const[appointments, setAppointments] = useState([])
   const[vinSearch, setVinSearch] = useState('')

   const fetchData = async () => {
        const Url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(Url)
            if (response.ok) {
                const data = await response.json()
                setAppointments(data['appointments'])
            }
    }
    useEffect(() => {fetchData()}, [])


   const handleVinSearchChange = event => setVinSearch(event.target.value)
   const handleSearchButtonChange = () => setVinSearch('')

    return (
    <div>
        <input type="text" placeholder="Search by VIN" value={vinSearch} onChange={handleVinSearchChange} />
        <button onClick={handleSearchButtonChange} type="button" className="btn btn-primary">
        Clear
        </button>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.filter(appointment => vinSearch ?  appointment.vin.includes(vinSearch) : appointment)
                .map(appointment => {
                return(
                <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.customer}</td>
                    <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                    <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                    <td>{appointment.technician}</td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.status}</td>
                </tr>
                )

            })}
            </tbody>
        </table>
    </div>
)
}
export default ServiceHistory

from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)


class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now=False, auto_now_add=True)
    reason = models.TextField()
    status = models.CharField(max_length=50, default="In Progress")
    vin = models.CharField(max_length=50)
    vip = models.BooleanField(default=False, null=True, blank=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        null=True,
        on_delete=models.PROTECT
    )

    def cancel(self):
        self.status = "canceled"
        self.save()

    def complete(self):
        self.status = "finished"
        self.save()
